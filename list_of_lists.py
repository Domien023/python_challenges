import math
def enrollment_stats(list):
    i = 0
    students_in_univ = []
    sum_tuition = []
    for num in range(0, len(list)):
        students_in_univ.append(list[i][1])
        sum_tuition.append(list[i][2])
        i += 1
    return sum(students_in_univ), sum(sum_tuition)
def mean(list):
    i = 0
    students_in_univ = []
    sum_tuition = []
    for num in range(0, len(list)):
        students_in_univ.append(list[i][1])
        sum_tuition.append(list[i][2])
        i += 1
    student_mean = sum(students_in_univ)/7
    tuition_mean = sum(sum_tuition)/7
    return student_mean, tuition_mean
def median(list):
    i = 0
    students_in_univ = []
    sum_tuition = []
    for num in range(0, len(list)):
        students_in_univ.append(list[i][1])
        sum_tuition.append(list[i][2])
        i += 1
    students_in_univ.sort()
    sum_tuition.sort()
    if len(students_in_univ) % 2 == 0:
        median_index_1 = int(len(students_in_univ)/2)
        median_index_2 = int((len(students_in_univ)/2) - 1)
        median_students = (students_in_univ[median_index_1] + students_in_univ[median_index_2]) / 2
        median_tuitions = (sum_tuition[median_index_1] + sum_tuition[median_index_2] / 2)
        return median_students, median_tuitions
    else:
        index_student_median = math.floor(len(students_in_univ)/2)
        index_tuition_median = math.floor(len(sum_tuition)/2)
        return students_in_univ[index_student_median], sum_tuition[index_tuition_median]
universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]
print(f"""****************************
Total students: {enrollment_stats(universities)[0]}
Total tuition: $ {enrollment_stats(universities)[1]:,}

Student mean: {mean(universities)[0]:,.2f}
Student median: {median(universities)[0]:,}

Tuition mean: {mean(universities)[1]:,.2f}
Tuition median: {median(universities)[1]:,}
****************************
""")

