import random
def toss_coin():
    return random.randint(0, 1)
num_of_toss = 0
for trial in range(10000):
    head = 0
    tail = 0
    while (head == 0) or (tail == 0):
        if toss_coin() == 0:
            tail += 1
            num_of_toss += 1
        else:
            head += 1
            num_of_toss += 1
print(f"Average of trials is {(num_of_toss/10000):.2f}")
