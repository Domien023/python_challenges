import tkinter as tk
from tkinter.filedialog import asksaveasfilename
import random

def input_words():
    nouns = ent_nouns.get().split(",")
    if len(nouns) != 3:
        lbl_poem["text"] = f"Error!\nWrong amount entered words!"
        return
    random.shuffle(nouns)
    verbs = ent_verbs.get().split(",")
    if len(verbs) != 3:
        lbl_poem["text"] = f"Error!\nWrong amount entered words!"
        return
    random.shuffle(verbs)
    adjectives = ent_adjectives.get().split(",")
    if len(adjectives) != 3:
        lbl_poem["text"] = f"Error!\nWrong amount entered words!"
        return
    random.shuffle(adjectives)
    prepositions = ent_prepositions.get().split(",")
    if len(prepositions) != 3:
        lbl_poem["text"] = f"Error!\nWrong amount entered words!"
        return
    random.shuffle(prepositions)
    adverbs = ent_adverbs.get()
    if "," in adverbs:
        lbl_poem["text"] = f"Error!\nWrong amount entered words!"
        return

    lbl_poem["text"] = f"""Your poem:\n\nA {adjectives[0]} {nouns[0]}\n\n
A {adjectives[0]} {nouns[0]} {verbs[0]} {prepositions[0]} the {adjectives[1]} {nouns[1]}\n
{adverbs}, the {nouns[0]} {verbs[1]}\n
the {nouns[1]} {verbs[2]} {prepositions[1]} a {adjectives[2]} {nouns[2]}."""

def save_as():
    save_file = asksaveasfilename(
        defaultextension="txt",
        filetypes=[("Text Files", "*.txt"), ("All Files", "*.*")]
    )
    if not save_file:
        return
    with open(save_file, "w") as file_output:
        poem = lbl_poem["text"]
        file_output.write(poem)

window = tk.Tk()
window.title("Make your own poem!")
window.rowconfigure(0, weight=1)
window.columnconfigure(0, weight=1)
lbl_enter = tk.Label(master=window, text="Enter your favourite words, separated by commas.")
lbl_enter.grid(row=0, column=0, pady=5)

frm_input = tk.Frame(master=window)
frm_input.grid(row=1, column=0)
lbl_nouns = tk.Label(master=frm_input, text="Nouns: ")
lbl_nouns.grid(row=0, column=0, sticky="e")
ent_nouns = tk.Entry(master=frm_input, width=65)
ent_nouns.grid(row=0, column=1, sticky="e")
lbl_verbs = tk.Label(master=frm_input, text="Verbs: ")
lbl_verbs.grid(row=1, column=0, sticky="e")
ent_verbs = tk.Entry(master=frm_input, width=65)
ent_verbs.grid(row=1, column=1, sticky="e")
lbl_adjectives = tk.Label(master=frm_input, text="Adjectives: ")
lbl_adjectives.grid(row=2, column=0, sticky="e")
ent_adjectives = tk.Entry(master=frm_input, width=65)
ent_adjectives.grid(row=2, column=1, sticky="e")
lbl_prepositions = tk.Label(master=frm_input, text="Prepositions: ")
lbl_prepositions.grid(row=3, column=0, sticky="e")
ent_prepositions = tk.Entry(master=frm_input, width=65)
ent_prepositions.grid(row=3, column=1, sticky="e")
lbl_adverbs = tk.Label(master=frm_input, text="Adverbs: ")
lbl_adverbs.grid(row=4, column=0, sticky="e")
ent_adverbs = tk.Entry(master=frm_input, width=65)
ent_adverbs.grid(row=4, column=1, sticky="e")

btn_generate = tk.Button(master=window, text="Generate", command=input_words)
btn_generate.grid(row=2, column=0, pady=10)

frm_output = tk.Frame(master=window, relief=tk.GROOVE, borderwidth=4)
frm_output.grid(row=3, column=0, pady=5, padx=5)
lbl_poem = tk.Label(master=frm_output, text="", width=65, height=10)
lbl_poem.grid(row=0, column=0)
btn_save = tk.Button(master=frm_output, text="Save as", command=save_as)
btn_save.grid(row=1, column=0, pady=10)

window.mainloop()
