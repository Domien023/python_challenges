def invest(amount, rate, year):
    amount = (amount * rate) + amount
    print(f"year {year}: ${amount:.2f}")
user_year = input("Number of year: ")
user_amount = input("Principal amount: ")
user_amount = int(user_amount)
user_rate = input("An annual percentage rate: ")
user_rate = float(user_rate)
for num_year in range(1, int(user_year)+1):
    invest(user_amount, user_rate, num_year)
    user_amount = (user_amount * user_rate) + user_amount
