import random
capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
}
states = []
for state, capital in capitals_dict.items():
    states.append(state)
rand_state = random.choice(states)
rand_capital = capitals_dict[rand_state]
user_capital = input(f"Guess the capital of {rand_state} state: ").lower()

if user_capital == "exit":
    print("Goodbye")
else:
    while user_capital != rand_capital.lower():
        print("Wrong capital. Try again")
        user_capital = input().lower()
    print("Correct")
