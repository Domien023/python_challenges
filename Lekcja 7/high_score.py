from pathlib import Path
import csv
high_scores = []
help_dict = dict()
scores = []
def sort_value(value):
    return value['score']
def int_score(line):
    line['score'] = int(line['score'])
    return line

file_path = Path.home() / "python-basics-exercises-master" / "ch12-file-input-and-output" / "practice_files" / "scores.csv"
new_file_path = Path.home() / "python-basics-exercises-master" / "ch12-file-input-and-output" / "practice_files" / "high_scores.csv"
with file_path.open("r", newline="") as file:
    reader = csv.DictReader(file)
    for line in reader:
        scores.append(int_score(line))

scores = sorted(scores, key=sort_value, reverse=True)

for row in scores:
    if row['name'] not in help_dict:
        help_dict[row['name']] = row['score']
        high_scores.append(row)
    else:
        continue

with new_file_path.open("w", newline="") as new_file:
    writer = csv.DictWriter(new_file, fieldnames=["name", "score"])
    writer.writeheader()
    writer.writerows(high_scores)
