from pathlib import Path
practice_path = Path.home()  / "python-basics-exercises-master" / "ch12-file-input-and-output" / "practice_files"
images = practice_path / "images"
list_of_image = []
extensions = ["*.jpg", "*.png", "*.gif"]
for extension in extensions:
    for path in practice_path.rglob(extension):
        list_of_image.append(path)
for source in list_of_image:
    destination = images / source.name
    source.replace(destination)
