import random
def elections(chance):
    if random.random() < chance:
        return 1
    else:
        return 0
wins_can_a = 0
wins_can_b = 0
for i in range(10000):
    can_a = 0
    can_b = 0
    if elections(.87) == 1:
        can_a += 1
    else:
        can_b += 1
    if elections(.65) == 1:
        can_a += 1
    else:
        can_b += 1
    if elections(.17) == 1:
        can_a += 1
    else:
        can_b += 1
    if can_a > can_b:
        wins_can_a += 1
    else:
        wins_can_b += 1
print(f"Candidate A wins in {wins_can_a/10000:.2%}")
print(f"Candidate B wins in {wins_can_b/10000:.2%}")
