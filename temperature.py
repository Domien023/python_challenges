def convert_cel_to_far(cel):
    far = cel * (9/5) + 32
    return far
def convert_far_to_cel(far):
    cel = (far-32) * 5/9
    return cel
user_far = input("Enter a temperature in degrees F: ")
user_cel = convert_far_to_cel(float(user_far))
print(f"{user_far} degrees F = {user_cel:.2f} degrees C")
user_cel = input("Enter a temperature in degrees C: ")
user_far = convert_cel_to_far(float(user_cel))
print(f"{user_cel} degrees C = {user_far:.2f} degrees F")
