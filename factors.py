user_num = int(input("Enter a positive number: "))
num = 0
if user_num >= 0:
    while num <= user_num:
        num += 1
        if (user_num % num) == 0:
            print(f"{num} is a factor of {user_num}")
else:
    print("It was supposed to be a positive number")
