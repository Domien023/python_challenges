from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

class PdfFileSplitter:
    def __init__(self, file):
        self.file = file
        self.path = Path.home() / file
        self.writer1 = PdfFileWriter()
        self.writer2 = PdfFileWriter()

    def split(self, breakpoint):
        breakpoint -= 1
        reader = PdfFileReader(str(self.path))
        for page1 in reader.pages[:breakpoint]:
            self.writer1.addPage(page1)
        for page2 in reader.pages[breakpoint:]:
            self.writer2.addPage(page2)

    def write(self, filename):
        new_pdf1 = Path.home() / (filename + "_1.pdf")
        new_pdf2 = Path.home() / (filename + "_2.pdf")
        with open(new_pdf1, "wb") as file1:
            self.writer1.write(file1)
        with open(new_pdf2, "wb") as file2:
            self.writer2.write(file2)

pdf_splitter = PdfFileSplitter("Pride_and_Prejudice.pdf")
pdf_splitter.split(150)
pdf_splitter.write("Pride_and_Prejudice")
