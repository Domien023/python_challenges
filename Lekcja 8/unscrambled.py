from PyPDF2 import PdfFileReader, PdfFileWriter
from pathlib import Path

pdf_path = (
    Path.home() /
    "python-basics-exercises-master" /
    "ch14-interact-with-pdf-files" /
    "practice_files" /
    "scrambled.pdf")

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()

text_list = []
for page in pdf_reader.pages:
    text = page.extractText()
    text_list.append(text)

for n in sorted(text_list):
    for page in pdf_reader.pages:
        if page.extractText() == n:
            deg = page["/Rotate"]
            page.rotateClockwise(-deg)
            pdf_writer.addPage(page)

output_path = pdf_path.parent / "unscrambled.pdf"
with open(output_path, "wb") as output:
    pdf_writer.write(output)
