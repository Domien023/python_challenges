class Animals:
    farm_goods = [0, 0, 0, 0]
    def __init__(self, animal, amount, size, goods, amount_harvest):
        self.animal = animal
        self.amount = amount
        self.size = size
        self.goods = goods
        self.amount_harvest = amount_harvest
    def welcome(self):
        return f"Welcome on our farm. We have here many animals"
    def harvest(self):
        self.farm_goods[0] += self.amount_harvest
        return f"Today {self.amount} {self.size} size {self.animal}'s gave us {self.amount_harvest} {self.goods}s"
    def shopping(self, amount):
        self.amount += amount
        if amount > 1:
            s = "s"
        else:
            s = ""
        return f"We bought {amount} {self.animal}{s}, now we have {self.amount}"

class Dog(Animals):
    def sound(self, name, sound="*bark*"):
        return f"Here is a {self.animal}, his name is {name}, and he says {sound}"
class Chickens(Animals):
    pass
class Sheeps(Animals):
    pass
class Pigs(Animals):
    def bacon(self):
        return f"{self.amount} {self.size} size {self.animal}'s wont give us goods as fast as the other animals :)"


bob = Dog("dog", 1, "medium", None, 0)
chicken = Chickens("chicken", 20, "small", "egg", 20)
sheep = Sheeps("sheep", 7, "medium", "wool", 200)
pig = Pigs("pig", 14, "medium", "bacon", 0)

print(chicken.welcome())
print(bob.sound("Bob"))
print(chicken.harvest())
print(sheep.harvest())
print(pig.bacon())
print(pig.shopping(1))
print(chicken.shopping(2))
