cats = {}
cats_hats = []
for num in range(1, 101):
    cats[f"cat{num}"] = 0 # create 100 cats with no hats
for round in range(1, 101):
    for num_cat in range(1, 101):
        if cats[f"cat{num_cat}"] == 0 and num_cat % round == 0:
            cats[f"cat{num_cat}"] = 1
        elif cats[f"cat{num_cat}"] == 1 and num_cat % round == 0:
            cats[f"cat{num_cat}"] = 0
for i in range(1, 101):
    if cats[f"cat{i}"] == 1:
        cats_hats.append(i)

print(f"Cats with hats: {cats_hats}")
