import random
def poem():
    noun_1 = random.choice(nouns)
    noun_2 = random.choice(nouns)
    noun_3 = random.choice(nouns)
    while noun_1 == noun_2:
        noun_2 = random.choice(nouns)
    while noun_1 == noun_3 or noun_2 == noun_3:
        noun_3 = random.choice(nouns)

    verb_1 = random.choice(verbs)
    verb_2 = random.choice(verbs)
    verb_3 = random.choice(verbs)
    while verb_1 == verb_2:
        verb_2 = random.choice(verbs)
    while verb_1 == verb_3 or verb_2 == verb_3:
        verb_3 = random.choice(verbs)

    adj_1 = random.choice(adjectives)
    adj_2 = random.choice(adjectives)
    adj_3 = random.choice(adjectives)
    while adj_1 == adj_2:
        adj_2 = random.choice(adjectives)
    while adj_1 == adj_3 or adj_2 == adj_3:
        adj_3 = random.choice(adjectives)

    prep_1 = random.choice(prepositions)
    prep_2 = random.choice(prepositions)
    while prep_1 == prep_2:
        prep_2 = random.choice(prepositions)

    adverb1 = random.choice(adverbs)

    letters = ["a", "i", "e", "o", "u"]
    if adj_1[0] in letters:
        first_letter = "An"
    else:
        first_letter = "A"

    text = (f"""{first_letter} {adj_1} {noun_1}

{first_letter} {adj_1} {noun_1} {verb_1} {prep_1} the {adj_2} {noun_2}
{adverb1}, the {noun_1} {verb_2}
the {noun_2} {verb_3} {prep_2} a {adj_3} {noun_3}""")
    return text
nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

print(poem())
